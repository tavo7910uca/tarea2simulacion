#include <fstream>
#include <iostream>
#include "classes.h"

using namespace std;

void readCoordinates(ifstream &file, int n, item* item_list){
    int lineaFile; float elementFileFloat;

    for(int i=0; i<=n; i++){
       file >> lineaFile >> elementFileFloat;

      //file >> elementFileFloat >> lineaFile;
      item_list[i].setIntFloat(lineaFile,elementFileFloat);
      cout <<"Elemento " << lineaFile << "- x: " << elementFileFloat << "\n";
    }
}


int main() {

    char filename[10];
    string line;
    mesh m;
    ifstream file;
    float k,Q;
    int nnodes,neltos,ndirich,nneu;
    
    
    do{
        cout << "Ingrese el nombre del archivo: ";
        cin >> filename;
        file.open(filename);
    }while(!file);
    
    file >> k >> Q;
    file >> nnodes >> neltos >> ndirich >> nneu;
    //ndirich y nneu son condiciones de contorno 
    file >> line;

    m.setParameters(k, Q);
    m.setSizes(nnodes, neltos, ndirich, nneu);
    m.createData();

    readCoordinates(file,nnodes,m.getNodes());

    file.close();


  /*  for (int i = 0; i < nnodes; i++)
    {
            nodes[i].setIntFloat(nnodes, i);
            nodesInt = m.getNode(i).getId();
            cout <<"Elemento " << i << " " << nodesInt << nodes[i].getId() << "\n";
    }
    */
    //nodo es un punto en la recta numerica


    return 0;
}
